
#include "BioEnemyShip.h"


BioEnemyShip::BioEnemyShip()
{
	SetSpeed(150);
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}


void BioEnemyShip::Update(const GameTime *pGameTime)
{
	if (IsActive())
	{
		//if the BioEnemyship is active move its position based on the legth of time since the game started?
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		//set how far the BioEnemyShip will move back and forth along the x-axis
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 3.0f; //changed this just for fun, was 1.4f
		//actually move the BioEnemyShip
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());
		//if the BioEnemyShip is not on the screen Deactivate it
		if (!IsOnScreen()) Deactivate();
	}
	//do anything that an EnemyShip would do, Fire, Hit, Collide etc.
	
	EnemyShip::Update(pGameTime);
}


void BioEnemyShip::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}
