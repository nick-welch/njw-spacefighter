
#pragma once

#include "Weapon.h"

class Blaster : public Weapon
{

public:

	Blaster(const bool isActive) : Weapon(isActive)
	{
		m_cooldown = 0;
		//Change the player weapon's cooldowntime to allow the ship to shoot faster (was .35)
		m_cooldownSeconds = 0.05;
	}

	virtual ~Blaster() { }

	virtual void Update(const GameTime *pGameTime)
	{
		if (m_cooldown > 0) m_cooldown -= pGameTime->GetTimeElapsed();
	}

	virtual bool CanFire() const { return m_cooldown <= 0; }

	virtual void ResetCooldown() { m_cooldown = 0; }

	virtual float GetCooldownSeconds() { return m_cooldownSeconds; }

	virtual void SetCooldownSeconds(const float seconds) { m_cooldownSeconds = seconds; }

	virtual void Fire(TriggerType triggerType)
	{
		//is the blaster that called the fire method active, and does its cooldown allow it to fire?
		if (IsActive() && CanFire())
		{
			//get the triggertype of the weapon if there is one EX: Primary, Secondary, ALL
			if (triggerType.Contains(GetTriggerType()))
			{
				//create a new projectile to be fired?
				Projectile *pProjectile = GetProjectile();
				if (pProjectile)
				{
					//Activate the projectile object to cause it to fire
					pProjectile->Activate(GetPosition(), true);
					//set the cooldown of the blaster.
					m_cooldown = m_cooldownSeconds;
				}
			}
		}
	}


private:

	float m_cooldown;
	float m_cooldownSeconds;

};